# My Codes and copy paste for programming problems

It's just for learning purposes and have some fun understanding alien programming

## Getting Started in alien programming codes

![alt tag](http://sxh1b2g2g4f2w04gm2piih1u.wpengine.netdna-cdn.com/wp-content/uploads/2016/05/11.-Alien-1-696x464.jpg)


## Authors

* **Rodrigo Cardona Q** - *Initial work* - [PurpleBooth](https://github.com/PurpleBooth)

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* A mi tia
* A mi abuelita
